FROM neilpang/acme.sh@sha256:34b504050f484dfa86a746555239d561865d8cf65454aece982d39c14250ca1a

ENV AUTO_UPGRADE "0"

ADD https://raw.githubusercontent.com/acmesh-official/acme.sh/f0e96bf32876567a536e7f1704459808ea3efdda/dnsapi/dns_aws.sh /root/.acme.sh/dnsapi/dns_aws.sh